FROM maven:3.8-openjdk-17-slim
WORKDIR /src
COPY . .
RUN mvn clean package

#rename file so it doesn't depend on version
run mv target/rest_api-*.jar target/app.jar

#deploy
ARG REST_API_DEPLOY_DEST='development'
ENV REST_API_DEPLOY_DEST=${REST_API_DEPLOY_DEST}
EXPOSE 8090
ENTRYPOINT ["java","-jar","target/app.jar","--spring.profiles.ctive=$REST_API_DEPLOY_DEST"]